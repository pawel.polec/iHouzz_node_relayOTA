#define DEBUG_ESP_SSL 1

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <LittleFS.h>
#include "ESP8266httpUpdate.h"
#include <ArduinoJson.h>
#include <string>
#include "config.h"
#include "httpSrv.h"

#include <sntp.h>
#include <TimeLib.h>
#include <TimeAlarms.h>
#include <coredecls.h>

const char* host = "ihouzz.ddns.net";
const int httpsPort = 443;

const char* url = "/fw.php?type=relay_v1";

const char* APssid = "iHouzzNodeRelay";

#define LED (2)

#define currentVersion "1.0.4"
void setupAP() {
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(IPAddress(192, 168, 0, 1), IPAddress(192, 168, 0, 1), IPAddress(255, 255, 255, 0));   // subnet FF FF FF 00
  WiFi.softAP(APssid);

  IPAddress myIP = WiFi.softAPIP(); //Get IP address
  Serial.println("HotSpot IP:" + myIP.toString());
}

WiFiClientSecure client;

void setup() {
  Serial.begin(115200);

  pinMode(LED, OUTPUT); //turn off LED on ESP-01s
  digitalWrite(LED, HIGH);
//  Serial.setDebugOutput(true);

  if(!LittleFS.begin()){
    Serial.println("An Error has occurred while mounting LittleFS");
    return;
  }

  delay(1000);

  bool setAP = true;
  if (Config::getInstance().getSSID() != "") {
    Serial.print("connecting to " + Config::getInstance().getSSID());
    WiFi.mode(WIFI_STA);
    if (WiFi.SSID() != Config::getInstance().getSSID()) {
      WiFi.hostname("RelayNode");
      WiFi.begin(Config::getInstance().getSSID(), Config::getInstance().getPASS());
    }

    for (int attempts = 0; attempts < 30; ++attempts) {
      delay(500);
      Serial.print(".");
      if (WiFi.status() == WL_CONNECTED) {
        setAP = false;
        break;
      }
    }
  }

  if (setAP) {
    setupAP();
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // configure time
  configTime(3600, 0, "pool.ntp.org");
  settimeofday_cb([] {setTime(sntp_get_current_timestamp() + 3600);});

  // Use WiFiClientSecure class to create TLS connection
  //BearSSL::X509List x509(x509CA);

  //client.setTrustAnchors(&x509);
  client.setInsecure();
  yield();
  Serial.print("connecting to ");
  Serial.println(host);
  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    return;
  }
  yield();

  // if (client.verify(fingerprint, host)) {
  //   Serial.println("certificate matches");
  // } else {
  //   Serial.println("certificate doesn't match");
  //   return;
  // }

  Serial.print("Starting OTA from: ");
  Serial.println(url);
  
  yield();
  //auto ret = ESPhttpUpdate.update(client, host, url);
  auto ret = ESPhttpUpdate.update(client, host, httpsPort, url, currentVersion);
  // if successful, ESP will restart
  yield();
  Serial.println("update failed");
  Serial.println((int) ret);
  yield();
}

void printTime(time_t *tim){
  tmElements_t tms = {0};// *timeinfo = localtime(tim);
  breakTime(now(), tms);
  char s[] = "00:00:00";

  snprintf(s, sizeof(s), "%02d:%02d:%02d", tms.Hour%24, tms.Minute%60, tms.Second%60);

  Serial.println(s);

}

void loop() {
  static time_t old_ti = 0;
  time_t ti;
  time (&ti);

  if (old_ti != ti) {
    printTime(&ti);
    old_ti = ti;
    if (ti%36000 == 0) ESPhttpUpdate.update(client, host, httpsPort, url, currentVersion);
  }
  yield();

  HttpSrv::getInstance().handleRequest();
  
  // struct tm *timeinfo = localtime(&ti) ;
  // Serial.println(localtime(&ti)->tm_sec );
}
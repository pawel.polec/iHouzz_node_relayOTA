#include "scheduler.h"
#include <algorithm>


namespace Scheduler {

int Scheduler::registerAlarm(std::shared_ptr<Task> task) {
    for (auto & t: tasks) {
        if (t == nullptr) {
            t = task;
            return 1;
        }
    }
    return 0;
}

int Scheduler::unregisterAlarm(std::shared_ptr<Task> task) {
    int ret = 0;
    for (auto & t: tasks) {
        if (t == task) {
            t = nullptr;
            ++ret;
        }
    }
    return ret;
}

}
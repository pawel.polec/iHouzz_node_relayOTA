
#include <LittleFS.h>
#include "httpSrv.h"
#include "config.h"
#include <ArduinoJson.h>
#include <string>
constexpr int server_port = 80;

#define RELAY (0)
String state = "off";
HttpSrv::HttpSrv(bool modeIsConfig) : server{ESP8266WebServer(server_port)} {
  if (!LittleFS.exists("/html/bs.css")) {
    Serial.println("NOT EXIST!");
    if (!LittleFS.exists("/html")) {
      LittleFS.mkdir("/html");
    }
    LittleFS.open("/html/bs.css", "w").close();
  }

  pinMode(RELAY, OUTPUT);

  server.serveStatic("/bs.css", LittleFS, "/html/bs.css", "Content-Type: text/css");
  server.serveStatic("/bsg.css", LittleFS, "/html/bsg.css", "Content-Type: text/css");
  server.serveStatic("/bs.js", LittleFS, "/html/bs.js", "Content-Type: text/javascript");
  server.serveStatic("/jq.js", LittleFS, "/html/jq.js", "Content-Type: text/javascript");

  server.serveStatic("/wifi.svg", LittleFS, "/html/wifi.svg", "Content-Type: image/svg+xml");
  server.serveStatic("/wifi-3.svg", LittleFS, "/html/wifi.svg", "Content-Type: image/svg+xml");
  server.serveStatic("/wifi-1.svg", LittleFS, "/html/wifi-1.svg", "Content-Type: image/svg+xml");
  server.serveStatic("/wifi-2.svg", LittleFS, "/html/wifi-2.svg", "Content-Type: image/svg+xml");

  server.serveStatic("/config", LittleFS, "/config.json");
  server.serveStatic("/", LittleFS, "/html/index.html");

  server.on("/post", HTTP_POST, [&] {
    server.send ( 100, "text/plain", "");
    int args= server.args();
    Serial.println (String("Args no: ") + args);

    for (int i = 0; i< args; ++i) {
      Serial.println(server.argName(i) + " " + server.arg(i));
    }

    if (server.hasArg("ssid")) {
      Serial.println(server.arg("ssid") + " " + server.arg("pass"));
      Config::getInstance().setSSID(server.arg("ssid"));
      if (server.hasArg("pass"))
        Config::getInstance().setPASS(server.arg("pass"));
      else
        Config::getInstance().setPASS("");
      Config::getInstance().save();
    }

    server.sendHeader("Location", String("/"), true);
    server.send ( 302, "text/plain", "");
  });
  server.on("/relay", HTTP_POST, [&](){
      Serial.println("Set request handling");
      StaticJsonDocument<100> doc;
      Serial.println(server.arg("plain"));
      deserializeJson(doc, server.arg("plain"));
      state = String(doc["state"]);
      if (state == "on") {
        Serial.println("Set state: ON");
        digitalWrite(RELAY, LOW); //should be moved to separate class: RelayControl
      } else {
        Serial.println("Set state: OFF");
        digitalWrite(RELAY, HIGH);
        state = "off";
      }
      server.send ( 200, "application/json", "{\"state\":\"" + state + "\"}");
  });
  server.on("/relay", HTTP_GET, [&](){
    server.send ( 200, "application/json", "{\"state\":\"" + state + "\"}");
  });

  server.on("/ssidlist", [&] {
    int numberOfNetworks = WiFi.scanNetworks();

    String nets = {""};
    for ( int i = 0; i<numberOfNetworks; ++i){
      nets += String("{\"ssid\":\"")+WiFi.SSID(i)+"\",\"rssi\":" + WiFi.RSSI(i) + "}";
      if (i<(numberOfNetworks-1))
        nets += String(",");
    }

    server.send ( 200, "application/json", "{\"count\":" + String(numberOfNetworks) + ",\"list\":[" + nets + "]}");
  });
  // server.on("/get", [&]() {
  //   server.serveStatic("/bs.css", LittleFS, "/html/bs.css", "");
  // });

  server.onNotFound([&]() {
    server.send(404, "text/html", "<html><head><title>iHouzz node 404</title></head><body><h1>404</h1><p>404 - Page not found!</p></body></html>");
  });

  server.begin();
}

int HttpSrv::handleRequest() {
  server.handleClient();
  return 0;
}

#ifndef CONFIG_H
#define CONFIG_H


#include <string>
#include <IPAddress.h>

class Config {
    public:
        static Config& getInstance()
        {
            static Config instance;
            return instance;
        }

        String getSSID() const { return ssid;}
        String getPASS() const { return pass;}

        void setSSID(const String &s) { ssid = s;}
        void setPASS(const String &s) { pass = s;}

        void save();
    private:
        Config();

        String ssid;
        String pass;

    public:
        Config(Config const&)         = delete;
        void operator=(Config const&) = delete;

        // Note: Scott Meyers mentions in his Effective Modern
        //       C++ book, that deleted functions should generally
        //       be public as it results in better error messages
        //       due to the compilers behavior to check accessibility
        //       before deleted status
};

#endif
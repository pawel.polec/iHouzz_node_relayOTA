#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <vector>
#include <memory>
#include <functional>

namespace Scheduler {

class Scheduler;

class Task {
    public:
    friend class Scheduler;
    Task(std::function<void()> callback, long secOfADay, long intervalSec = 0);

    Task() = delete;
    Task(Task &) = delete;
    Task& operator=(Task&) = delete;

    private:
    long interval = 0;
    long nextTimestap = 0;
    std::function<void()> exec = nullptr;

};

class Scheduler {
    public:
    int run();
    int registerAlarm(std::shared_ptr<Task> task );
    int unregisterAlarm(std::shared_ptr<Task> task);
    Scheduler();

    private:
    std::vector<std::shared_ptr<Task>> tasks = std::vector<std::shared_ptr<Task>>(10, nullptr);
};

}


#endif //SCHEDULER_H